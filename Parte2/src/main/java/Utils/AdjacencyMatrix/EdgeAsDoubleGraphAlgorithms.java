
package utils.AdjacencyMatrix;

import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class EdgeAsDoubleGraphAlgorithms {

    /**
     * Determine the shortest path to all vertices from a vertex using Dijkstra's algorithm
     * To be called by public short method
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex 
     * @param knownVertices previously discovered vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param minDist minimum distances in the path
     *
     */
    private static <V> void shortestPath(AdjacencyMatrixGraph<V,Double> graph, int sourceIdx, boolean[] knownVertices, int[] verticesIndex, double [] minDist)	{  
        for(V v : graph.vertices) {
            minDist[graph.toIndex(v)] = Double.MAX_VALUE;
            verticesIndex[graph.toIndex(v)] = -1;
            knownVertices[graph.toIndex(v)] = false;
        }
        minDist[sourceIdx] = 0;
        while(sourceIdx != -1) {
            knownVertices[sourceIdx] = true;
            for(V vAdj : graph.directConnections(graph.vertices.get(sourceIdx))) {
                double edge = graph.getEdge(graph.vertices.get(sourceIdx), vAdj);
                if(!knownVertices[graph.toIndex(vAdj)] && minDist[graph.toIndex(vAdj)] > minDist[sourceIdx] + edge) {
                    minDist[graph.toIndex(vAdj)] = minDist[sourceIdx] + edge;
                    verticesIndex[graph.toIndex(vAdj)] = sourceIdx;
                }
            }
            
            double min = Double.MAX_VALUE;
            V vAux = null;
            for(Object o : graph.vertices.toArray()) {
                V vAdj = (V)o;
                if(!knownVertices[graph.toIndex(vAdj)] && minDist[graph.toIndex(vAdj)] < min) {
                    min = minDist[graph.toIndex(vAdj)];
                    vAux = vAdj;
                }
            }
            sourceIdx = graph.toIndex(vAux);
        }

    }


    /**
     * Determine the shortest path between two vertices using Dijkstra's algorithm
     *
     * @param graph Graph object
     * @param source Source vertex 
     * @param dest Destination vertices
     * @param path Returns the vertices in the path (empty if no path)
     * @return minimum distance, -1 if vertices not in graph or no path
     *
     */
    public static <V> double shortestPath(AdjacencyMatrixGraph<V, Double> graph, V source, V dest, LinkedList<V> path){
        path.clear();
        if(!graph.checkVertex(source) || !graph.checkVertex(dest)) return -1;
        else if(source.equals(dest)) {
            path.add(dest);
            return 0;
        }
        
        int[] verticesIndex = new int[graph.numVertices];
        double[] minDistance = new double[graph.numVertices];
        boolean[] knownVertices = new boolean[graph.numVertices];
        
        shortestPath(graph, graph.toIndex(source), knownVertices, verticesIndex, minDistance);

        if(knownVertices[graph.toIndex(dest)] == false) return -1;
        
        recreatePath(graph, graph.toIndex(source), graph.toIndex(dest), verticesIndex, path);
        
        LinkedList<V> stack = new LinkedList<>();
        while(!path.isEmpty()) {
            stack.push(path.remove());
        }
        while(!stack.isEmpty()) {
            path.add(stack.pop());
        }
        
        return minDistance[graph.toIndex(dest)];
    }


    /**
     * Recreates the minimum path between two vertex, from the result of Dikstra's algorithm
     * 
     * @param graph Graph object
     * @param sourceIdx Source vertex 
     * @param destIdx Destination vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param Queue Vertices in the path (empty if no path)
     */
    private static <V> void recreatePath(AdjacencyMatrixGraph<V, Double> graph, int sourceIdx, int destIdx, int[] verticesIndex, LinkedList<V> path){

        path.add(graph.vertices.get(destIdx));
        if (sourceIdx != destIdx){
            destIdx = verticesIndex[destIdx];        
            recreatePath(graph, sourceIdx, destIdx, verticesIndex, path);
        }
    }

    /**
     * Creates new graph with minimum distances between all pairs
     * uses the Floyd-Warshall algorithm
     * 
     * @param graph Graph object
     * @return the new graph 
     */

    public static <V> AdjacencyMatrixGraph<V, Double> minDistGraph(AdjacencyMatrixGraph<V, Double> graph){
            throw new UnsupportedOperationException("Not supported yet.");	
    }


}
