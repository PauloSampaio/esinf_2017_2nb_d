package utils;

import controllers.GameProject;
import models.Local;
import models.Player;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;

/**
 * Classe utilitaria para importacao de dados do jogo
 */
public class DataImporter {
    private static final char DELIMITER = ',';
    private static final String ENCODING = "UTF-8";

    /**
     * Importa dados do jogo de um ficheiros
     * @param path path do ficheiro com dados de jogo
     * @param game objecto do jogo para carregar dados
     * @throws IOException expecao lancada quando ha erros de leitura
     */
    public void importGame(String path, GameProject game) throws IOException {
        File fx = new File(path);
        if (!fx.exists()) {
            throw new IOException();
        }
        try (
            FileInputStream fis = new FileInputStream(fx);
            Reader reader = new InputStreamReader(fis, ENCODING)) {
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withDelimiter(DELIMITER));

            boolean firstRecord = true;
            String reading = null;
            for (CSVRecord record : parser) {

                String localName, playerName, points;
                String currentRecord = record.get(0);
                if(currentRecord.equals("LOCAIS")
                        || currentRecord.equals("CAMINHOS")
                        || currentRecord.equals("PERSONAGENS")
                        || currentRecord.equals("ALIANÇAS")) {
                    reading = record.get(0);
                    continue;
                }
                switch(reading) {
                    /*
                     * Exercicio 1
                     * ALINEA A)
                     */
                    case "LOCAIS":
                        localName = record.get(0);
                        points = record.get(1);
                        game.addLocal(new Local(localName), Integer.parseInt(points));
                        break;
                    case "CAMINHOS":
                        String l1Name = record.get(0);
                        String l2Name = record.get(1);
                        points = record.get(2);
                        game.addWay(l1Name, l2Name, Integer.parseInt(points));
                        break;
                    /*
                     * Exercicio 2
                     * ALINEA A)
                     */
                    case "PERSONAGENS":
                        playerName = record.get(0);
                        points = record.get(1);
                        game.addPlayer(new Player(playerName), Integer.parseInt(points));
                        game.setPlayerLocal(new Player(playerName), new Local(record.get(2)));
                        break;
                    case "ALIANÇAS":
                        game.setAlliance(new Player(record.get(0))
                                , new Player(record.get(1))
                                , new Boolean(record.get(2))
                                , Double.parseDouble(record.get(3)));
                        break;
                }
            }
        }
    }
}
