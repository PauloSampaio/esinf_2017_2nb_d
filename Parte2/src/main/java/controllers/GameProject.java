package controllers;

import models.Local;
import models.Player;
import utils.AdjacencyMap.Edge;
import utils.AdjacencyMap.Graph;
import utils.AdjacencyMatrix.AdjacencyMatrixGraph;
import utils.AdjacencyMatrix.BasicGraph;
import utils.AdjacencyMatrix.EdgeAsDoubleGraphAlgorithms;
import utils.AdjacencyMatrix.GraphAlgorithms;
import utils.DataImporter;

import java.io.IOException;
import java.util.*;

/**
 * Classe usada como controller para uma sessao do jogo
 */
public class GameProject {
    private Set localsSet;              // conjunto de locais
    private Set playersSet;             // conjunto de jogadores
    private Map localPerPlayer;         // jogadores "donos" dos locais
    private Map playerPoints;           // pontos jogador
    private Map localPoints;            // dificuldade a entrar no local
    private AdjacencyMatrixGraph ways;  // caminhos entre locais
    private Graph alliances;

    public GameProject() {
        this.ways = new AdjacencyMatrixGraph<>();
        this.alliances = new Graph<>(false);
        this.localsSet = new HashSet<Local>();
        this.playersSet = new HashSet<Player>();
        this.localPerPlayer = new HashMap<Local, Player>();
        this.playerPoints = new HashMap<Player, Double>();
        this.localPoints = new HashMap<Local, Double>();
    }

    /**
     * Exercicio 1
     * ALINEA A)
     */
    /**
     * Metodo para adicionar um local ao jogo
     * Complexidade O(1)
     * @param l local a adicionar
     * @param p pontos do local a ser adicionado
     * @return
     */
    public boolean addLocal(Local l, int p) {
        boolean added = this.localsSet.add(l);  // insere local no set de locais
        if(added) {
            localPoints.put(l, p);              // insere e pontos de dificuldade associados
            ways.insertVertex(l);               // insere no map de adjacencias vertice
        }
        return added;
    }

    /**
     * Metodo para adicionar caminho entre 2 locais
     * Complexidade O(V)
     * @param l1 nome de local 1 a definir ramo
     * @param l2 nome de local 2 a definir ramo
     * @param p pontos do caminho
     * @return retorna boolean com resultado da operacao
     */
    public boolean addWay(String l1, String l2, double p) {
        Local local1 = null, local2 = null;
        int i = 0;
        for(Object o : localsSet) {
            Local l = (Local)o;
            if(l.getName().equals(l1)) {
                local1 = l;
                i++;
            }
            if(l.getName().equals(l2)) {
                local2 = l;
                i++;
            }
            if(i > 1) break;
        }
        return addWay(local1, local2, p);
    }

    /**
     * Metodo para adicionar caminho entre 2 locais
     * Complexidade O(1)
     * @param l1 local 1 a definir no ramo
     * @param l2 local 2 a definir no ramo
     * @param p pontos do caminho
     * @return retorna boolean com resultado da operacao
     */
    public boolean addWay(Local l1, Local l2, double p) {
        return this.ways.insertEdge(l1, l2, p);
    }

    /**
     * Exercicio 1
     * ALINEA B)
     */
    /**
     * Caminho com menor dificuldade entre os 2 locais indicados
     * Complexidade O(V^2)
     * @param l1 local origem
     * @param l2 local destino
     * @return retorna path entre os 2 locais
     */
    public List lowestDifficultyWay(Local l1, Local l2) {
        LinkedList path = new LinkedList<>();
        EdgeAsDoubleGraphAlgorithms.shortestPath(ways, l1, l2, path);
        return path;
    }

    /**
     * Exercicio 1
     * ALINEA C)
     */
    /**
     * Metodo que devolve lista com caminho de custo minimo para conquistar local
     * Complexidade O(V^3)
     * @param path path de caminho a conquistar o local indicado
     * @param p jogador que pretende conquistar local
     * @param l local a conquistar
     * @return retorna dificuldade de conquistar local
     */
    public double conquerLocal(LinkedList path, Player p, Local l) {
        path.clear();
        if(localPerPlayer.get(p) != null) {
            if (localPerPlayer.get(p).equals(l)) return 0;
        }

        ArrayList locals = new ArrayList();

        for(Object aux : localPerPlayer.keySet()) {
            if(localPerPlayer.get(aux).equals(p)) {
                locals.add(aux);
            }
        }
        
        Set paths = new HashSet<>();
        for(Object aux : locals) {
            LinkedList pathAux = new LinkedList();
            EdgeAsDoubleGraphAlgorithms.shortestPath(ways, aux, l, pathAux);
            paths.add(pathAux);
        }

        double min = Double.MAX_VALUE;
        for(Object aux : paths) {
            LinkedList paux = (LinkedList) aux;
            double auxMin = 0;
            for(int i = 0; i < paux.size() - 1; i++) {
                Local orig = (Local)paux.get(i);
                Local dest = (Local)paux.get(i+1);
                Player ownerNextLocal = (Player)localPerPlayer.get(dest);
                double wayStrength = (Double)ways.getEdge(orig, dest);
                double nextLocalStrength = ((Integer)localPoints.get(dest) * 1.0);
                if(ownerNextLocal != null) {
                    auxMin += (Double) playerPoints.get(ownerNextLocal)
                            + wayStrength
                            + nextLocalStrength;
                }
                else {
                    auxMin += wayStrength
                            + nextLocalStrength;
                }
            }

            if(auxMin < min)  {
                min = auxMin;
                paux.removeFirst();
                paux.removeLast();
                path.addAll(paux);
            }
        }

        return min;
    }

    /**
     * Verifica se um jogador pode conquistar um local
     * Complexidade O(V^3)
     * @param p jogador que quer conquistar local
     * @param l local a ser conquistado
     * @return retorna se consegue ou nao
     */
    public boolean playerCanConquer(Player p, Local l) {
        if(localPerPlayer.get(l) == null) return true;
        return (Double)playerPoints.get(p) >= conquerLocal(new LinkedList(), p, l);
    }

    /***************************************************************************************************
     * Exercicio 2
     * ALINEA A)
     */
    /**
     * Metodo para adicionar um jogador ao jogo
     * Complexidade O(V^2)
     * @param pl jogador a adicionar
     * @param p pontos do jogador
     * @return
     */
    public boolean addPlayer(Player pl, double p) {
        boolean added = this.playersSet.add(pl);
        if(added) {
            playerPoints.put(pl, p);
            alliances.insertVertex(pl);
        }
        return added;
    }

    /**
     * Define alianca entre 2 jogadores e visibilidade
     * Complexidade O(1)
     * @param player jogador 1
     * @param player1 jogador 2
     * @param visibility visibilidade de alianca
     * @param compatibilityFactor fator de compatibilidade
     */
    public boolean setAlliance(Player player, Player player1, Boolean visibility, double compatibilityFactor) {
        return setAlliance(alliances, player, player1, visibility, compatibilityFactor);
    }

    /**
     * Define alianca entre 2 jogadores para um grafo dado
     * Complexidade O(1)
     * @param g grafo a inserir alianca
     * @param player jogador 1
     * @param player1 jogador 2
     * @param visibility visibilidade da alianca
     * @param compatibilityFactor fator de compatibilidade
     * @return
     */
    private boolean setAlliance(Graph g, Player player, Player player1, Boolean visibility, double compatibilityFactor) {
        if(!playersSet.contains(player) || !playersSet.contains(player1)) return false;
        g.insertEdge(player, player1, visibility, compatibilityFactor);
        return true;
    }

    /**
     * Define "dono" de um local
     * Complexidade O(1)
     * @param p jogador a tornar "dono" do local
     * @param l local em que ira ser definido o novo "dono"
     * @return retorna se mapeamento foi efetuado com sucesso
     */
    public boolean setPlayerLocal(Player p, Local l) {
        /*
        if(!playersSet.contains(p) || !localsSet.contains(l)) {
            return false;
        }
        */
        localPerPlayer.put(l, p);
        return true;
    }

    /**
     * Exercicio 2
     * ALINEA B)
     */
    /**
     * Devolve todos os aliados de um determinado jogador
     * Complexidade O(VxN)
     * @param p jogador para verificar aliancas
     * @return retorna lista
     */
    public List getAlliances(Player p) {
        List allies = new ArrayList();
        for(Object v : alliances.adjVertices(p)) {
            allies.add(v);
        }
        return allies;
    }

    /**
     * Exercicio 2
     * ALINEA C)
     */
    /**
     * Identifica a aliança mais forte e devolve a respetiva informação
     * Jogadores que a estabeleram e a respetiva força dessa aliança.
     * Complexidade O(E)
     * @param list lista para carregar com elementos da alianca
     * @return retorna double com forca da alianca
     */
    public double strongestAlliance(List list) {
        double maxStrength = Double.MIN_VALUE;
        list.clear();

        for(Object obj : alliances.edges()) {
            Edge edge = (Edge) obj;
            double sumPlayersStrength = (Double)playerPoints.get(edge.getVOrig())
                    + (Double)playerPoints.get(edge.getVDest());
            double alliesStrength = sumPlayersStrength * edge.getWeight();

            if(alliesStrength > maxStrength) {
                maxStrength = alliesStrength;
                list.clear();
                list.add(edge.getVDest());
                list.add(edge.getVOrig());
            }

        }
        return maxStrength;
    }
    
    /**
     * Exercicio 2
     * ALINEA D)
     */
    /**
     * Define alianca entre 2 jogadores e visibilidade
     * Complexidade O(V^2)
     * @param player jogador 1
     * @param player1 jogador 2
     * @param visibility visibilidade de alianca
     * @return retorna fator de compatibilidade calculado
     */
    public double setAlliance(Player player, Player player1, boolean visibility) {
        return setAlliance(alliances, player, player1, visibility);
    }

    /**
     * Define alianca entre 2 jogadores num grafo dado
     * Complexidade O(V^2)
     * @param g grafo a usar
     * @param player jogador 1
     * @param player1 jogador 2
     * @param visibility visibilidade
     * @return retorna fator de compatibilidade calculado
     */
    private double setAlliance(Graph g, Player player, Player player1, boolean visibility) {
        BasicGraph alliancesNetwork = getAlliancesNet();

        double compatibilityFactor;
        Object check = alliancesNetwork.getEdge(player, player1);
        if(check != null) {
            LinkedList path = new LinkedList();
            utils.AdjacencyMap.GraphAlgorithms.shortestPath(g, player, player1, path);
            double sum = 0;
            int c = 0;
            for(int i = 0; i < path.size() - 1; i++) {
                Edge e =g.getEdge(path.get(i), path.get(i+1));
                if((Boolean)e.getElement()) {
                    sum += e.getWeight();
                    continue;
                }
                c++;
            }
            compatibilityFactor = sum / (path.size() - 1 - c);
            setAlliance(g, player, player1, visibility, compatibilityFactor);
        } else {
            compatibilityFactor = ((new Random()).nextDouble()%1);
            setAlliance(g, player, player1, visibility, compatibilityFactor);
        }
        return compatibilityFactor;
    }

    /**
     * Exercício 2
     * ALÍNEA E)
     */
    /**
     * Recria um novo grafo de alianças entre todos os jogadores
     * Complexidade O(V^4)
     * @return graph
     */
    public Graph setAllAliances() {
        Graph gAux = alliances.clone();

        for(Object v1 : gAux.vertices()) {
            Player p1 = (Player)v1;
            for(Object v2 : gAux.vertices()) {
                Player p2 = (Player)v2;
                if(!(p1).equals(p2) && alliances.getEdge(p1, p2) == null) {
                    setAlliance(gAux, p1, p2, true);
                }
            }
        }
        return gAux;
    }

    /***************************************************************************************************
     * Exercicio 3
     * ALINEA F)
     */
    /**
     * Verifica se em alianca consegue conquistar um local
     * Complexidade O(V^3)
     * @param p jogador que quer conquistar local
     * @param p2 aliado
     * @param l local a conquistar
     * @param paths array com lista de caminhos intermedios de cada um dos jogadores
     * @return
     */
    public boolean conquerLocalWith(Player p, Player p2, Local l, double[] points, ArrayList paths) {
        paths.clear();
        if(localPerPlayer.get(p) != null || localPerPlayer.get(p2) != null) {
            if (localPerPlayer.get(p).equals(l)
                    || localPerPlayer.get(p2).equals(l)) return false;
        }

        LinkedList path1 = new LinkedList();
        double pointsP1 = conquerLocal(path1, p, l);

        LinkedList path2 = new LinkedList();
        double pointsP2 = conquerLocal(path2, p2, l);

        paths.add(path1);
        paths.add(path2);

        points[0] = pointsP1 + pointsP2;
        if(localPerPlayer.get(l) == null) return true;
        return (getPlayerPoints(p) + getPlayerPoints(p2)) >= points[0];
    }

    /***************************************************************************************************
     * AUXILIARES
     */

    /**
     * Obter todos os locais do jogo
     * Complexidade O(1)
     * @return retorna set com locais
     */
    public Set getLocals() {
        return localsSet;
    }

    /**
     * Retorna pontos de um local
     * Complexidade O(N)
     * @param l local para os quais se quer retornar pontos
     * @return retorna inteiro com numero de pontos
     */
    public int getLocalPoints(Local l) {
        if(!localsSet.contains(l)) {
            throw new NullPointerException("Vertice inexistente");
        }
        return (int) localPoints.get(l);
    }

    /**
     * Retorna pontos do caminho entre 2 locais
     * Complexidade O(1)
     * @param l1 local origem
     * @param l2 local destino
     * @return retorna inteiro com numero de pontos
     */
    public double getWayPoints(Local l1, Local l2) {
        if((!ways.checkVertex(l1) || !ways.checkVertex(l2))) {
            throw new NullPointerException("Vertice inexistente");
        }
        return (double) ways.getEdge(l1, l2);
    }

    /**
     * Retorna conjunto de jogadores
     * Complexidade O(1)
     * @return retorna set com jogadores
     */
    public Set getPlayers() {
        return playersSet;
    }

    /**
     * Retorna pontos de jogador
     * Complexidade O(N)
     * @param p jogador para os quais se pretende saber numero de pontos
     * @return retorna double com pontos
     */
    public double getPlayerPoints(Player p) {
        return (double) playerPoints.get(p);
    }

    /**
     * Retorna fator de compatibilidade entre 2 jogadores que tem uma alianca
     * Complexidade O(N)
     * @param p1 jogador 1
     * @param p2 jogador 2
     * @return retorna double com fator de compatibilidade
     */
    public double getAllianceStrength(Player p1, Player p2) {
        return alliances.getEdge(p1, p2).getWeight();
    }

    /**
     * Retorna matriz de adjacencias com rede de aliancas
     * Complexidade O(V^3)
     * @return retorna interface de grafo
     */
    public BasicGraph getAlliancesNet() {
        return getAlliancesNet(alliances);
    }

    /**
     * Verifica rede de aliancas par aum dado grafo
     * Complexidade O(V^3)
     * @param g grafo a verificar aliancas
     * @return retorna grafo com rede de aliancas
     */
    private BasicGraph getAlliancesNet(Graph g) {
        AdjacencyMatrixGraph alliancesNetwork = new AdjacencyMatrixGraph();
        // Construir matrix de adjacencias para rede
        for(Object vOrig : g.vertices()) {
            alliancesNetwork.insertVertex(vOrig);
            for(Object vAdj : g.adjVertices(vOrig)) {
                alliancesNetwork.insertEdge(vOrig, vAdj, g.getEdge(vOrig, vAdj).getElement());
            }
        }

        return GraphAlgorithms.transitiveClosure(alliancesNetwork, "Net");
    }

    /**
     * Retorna mapa de adjacencias com alianca
     * Complexidade O(1)
     * @return retorna grafo
     */
    public Graph getAlliances() {
        return this.alliances;
    }

    /**
     * Importa ficheiros para jogo
     * @param localFile path do ficheiro de locais
     * @param persFile path do ficheiro de jogadores
     * @throws IOException lanca excepcao de IO em caso de erro na leitura
     */
    public void importFiles(String localFile, String persFile) throws IOException {
        DataImporter importer = new DataImporter();
        importer.importGame(localFile, this);
        importer.importGame(persFile, this);
    }
}
