package models;

import java.util.Objects;

/**
 * Classe que define um jogador
 */
public class Player {
    private String name;

    /**
     * Construtor do jogador
     * @param name nome do jogador
     */
    public Player(String name) {
        this.name = name;
    }

    /**
     * Retorna nome do jogador
     * @return string com nome do jogador
     */
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return name.equals(player.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
