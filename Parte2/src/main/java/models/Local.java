package models;

import java.util.Objects;

/**
 * Classe que define um local
 */
public class Local {
    private String name;

    /**
     * Construtor do local
     * @param name nome do local
     */
    public Local(String name) {
        this.name = name;
    }

    /**
     * Obter nome do local
     * @return string com nome do local
     */
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Local)) return false;
        Local local = (Local) o;
        return name.equals(local.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
