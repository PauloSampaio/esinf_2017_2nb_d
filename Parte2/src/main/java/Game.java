import controllers.GameProject;
import models.Local;
import models.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by brunofigalves on 18/11/17.
 */
public class Game {
    public static void main(String[] args) {
        GameProject game = new GameProject();
        importFiles(game);
        String opt;
        do {
            opt = menu(game);
        } while(opt != "0");

        System.out.println("A sair...");
        System.exit(0);
    }

    /**
     * Menu do jogo
     * @param game instancia do jogo a correr
     * @return retorna ultima opcao de utilizador
     */
    private static String menu(GameProject game) {
        System.out.println("JOGO");
        System.out.println("1) Caminho com menor dificuldade entre dois locais. (Alinea 1b)");
        System.out.println("2) Verificar se uma determinada personagem pode conquistar um determinado local. (Alinea 1c)");
        System.out.println("3) Devolver uma lista com todos os aliados de uma dada personagem. (Alinea 2b)");
        System.out.println("4) Alianca mais forte, retornando a força e as personagens dessa aliança. (Alinea 2c)");
        System.out.println("5) Realizar uma nova alianca. (Alinea 2d))");
        System.out.println("6) Alianca entre todas as personagens. (Alinea 2e)");
        System.out.println("7) Conquistar local com aliado (Alinea 3f)");

        System.out.print("Opcao: ");
        Scanner sc = new Scanner(System.in);
        String opt = sc.nextLine();
        System.out.println();
        Player p = null, p2 = null;
        Local l1 = null, l2 = null;
        List list = null;
        LinkedList path = new LinkedList();
        LinkedList players = new LinkedList();
        switch(opt) {
            case "1":
                System.out.println("Caminhos:");
                System.out.println("Local 1: ");
                l1 = new Local(sc.nextLine());
                System.out.println("Local 2: ");
                l2 = new Local(sc.nextLine());
                list = game.lowestDifficultyWay(l1, l2);
                for(Object l : list) {
                    System.out.println(((Local)l).getName());
                }
                System.out.println();
                break;
            case "2":
                System.out.println("Player: ");
                p = new Player(sc.nextLine());
                System.out.println("Local: ");
                l1 = new Local(sc.nextLine());
                double points = game.conquerLocal(path, p, l1);
                System.out.println("Pontos necessarios para conquistar " + l1.getName() +": " + points);
                System.out.print("Locais intermedios a conquistar: ");
                if(path.size() == 0) System.out.println("Vazio");
                for(Object aux : path) {
                    System.out.println();
                    System.out.println(((Local)aux).getName());
                }
                System.out.println();
                break;
            case "3":
                System.out.println("Player: ");
                p = new Player(sc.nextLine());
                list = game.getAlliances(p);
                for(Object aux : list) {
                    System.out.println(((Player)aux).getName());
                }
                System.out.println();
                break;
            case "4":
                System.out.println("Forca alianca mais forte: " + game.strongestAlliance(players));
                for(Object aux : players) {
                    System.out.println(((Player)aux).getName());
                }
                System.out.println();
                break;
            case "5":
                System.out.println("Player 1: ");
                p = new Player(sc.nextLine());
                System.out.println("Player 2: ");
                p2 = new Player(sc.nextLine());
                System.out.println("Fator compatibilidade: "+game.setAlliance(p, p2, true));
                System.out.println();
                break;
            case "6":
                System.out.println(game.setAllAliances());
                System.out.println();
                break;
            case "7":
                System.out.println("Player 1: ");
                p = new Player(sc.nextLine());
                System.out.println("Player 2: ");
                p2 = new Player(sc.nextLine());
                System.out.println("Local: ");
                l1 = new Local(sc.nextLine());
                double pointsNeeded[] = new double[1];
                ArrayList listAux = new ArrayList<>();
                boolean result = game.conquerLocalWith(p, p2, l1, pointsNeeded, listAux);
                System.out.println("Consegue conquistar? " + result);
                System.out.println("Pontos necessarios: " + pointsNeeded[0]);
                System.out.println("Lista caminhos intermedios: ");
                System.out.println(listAux);
                System.out.println();
                break;
            default:
                opt = "0";
        }
        return opt;
    }

    /**
     * Importar ficheiros para jogo
     * @param game intancia de jogo para importar dados
     */
    public static void importFiles(GameProject game) {
        System.out.println("IMPORTACAO DE FICHEIROS");
        Scanner sc = new Scanner(System.in);
        System.out.println("Local path: ");
        String localFiles = sc.nextLine();
        System.out.println("Pers path: ");
        String persFiles = sc.nextLine();
        try {
            game.importFiles(localFiles, persFiles);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
