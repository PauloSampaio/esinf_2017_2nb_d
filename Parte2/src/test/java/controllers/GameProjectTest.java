package controllers;

import models.Local;
import models.Player;
import org.junit.Before;
import org.junit.Test;
import utils.AdjacencyMap.Graph;
import utils.AdjacencyMap.GraphAlgorithms;
import utils.AdjacencyMatrix.BasicGraph;
import utils.DataImporter;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by brunofigalves on 14/11/17.
 */
public class GameProjectTest {


    GameProject game;
    DataImporter importer;
    String sTestLocalsFile, sTestPeersFile;

    /**
     * Setup da classe de testes
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        game = new GameProject();
        importer = new DataImporter();
        sTestLocalsFile = "./src/test/java/files/S/locais_S.txt";
        sTestPeersFile = "./src/test/java/files/S/pers_S.txt";
        importer.importGame(sTestLocalsFile, game);
        importer.importGame(sTestPeersFile, game);
    }

    /**
     * Exercico 1)
     * ALINEA B)
     */

    /**
     * Testa devolucao de caminho minimo entre 2 localidades
     * @throws Exception
     */
    @Test
    public void lowestDifficultyWay() throws Exception {
        LinkedList result = (LinkedList) game.lowestDifficultyWay(new Local("Local0"), new Local("Local3"));

        // Teste caminho minimo quado 2 localidades estao ligadas
        // Caminho testado e minimo em pontos 20
        LinkedList expected = new LinkedList(Arrays.asList(new Local("Local0"), new Local("Local3")));
        assertEquals(result, expected);

        // Teste caminho com  3 localidades ligadas
        // Caminho testado tem peso minimo em pontos 20+20
        result = (LinkedList) game.lowestDifficultyWay(new Local("Local1"), new Local("Local19"));
        expected = new LinkedList(Arrays.asList(new Local("Local1")
                , new Local("Local16")
                , new Local("Local19")));
        assertEquals(result, expected);
    }

    /**
     * Exercicio 1)
     * ALINEA C)
     */

    /**
     * Testa se consegue conquistar um local, com um jogador passados como argumento
     * @throws Exception
     */
    @Test
    public void conquerLocal() throws Exception {
        LinkedList path = new LinkedList();

        // Teste pontos necessarios para Per0 conquistar Local3
        double result = game.conquerLocal(path, new Player("Pers0"), new Local("Local3"));
        double expected = game.getWayPoints(new Local("Local2"), new Local("Local3"))
                + game.getLocalPoints(new Local("Local3"));

        assertEquals(expected, result , 0.001);
        assertTrue(path.size() == 0);

        // Teste pontos necessarios para Pers0 conquistar Local14
        // Local14 tem "dono" Pers6
        result = game.conquerLocal(path, new Player("Pers0"), new Local("Local14"));
        expected = game.getWayPoints(new Local("Local2"), new Local("Local14"))
                + game.getLocalPoints(new Local("Local14")) + game.getPlayerPoints(new Player("Pers6"));

        assertEquals(expected, result,0.001);
        assertTrue(path.size() == 0);
        assertTrue(game.playerCanConquer(new Player("Pers0"), new Local("Local14")));

        // Teste pontos necessarios para Pers0 conquistar Local24
        // Local5 e caminho intermedio
        result = game.conquerLocal(path, new Player("Pers0"), new Local("Local24"));
        expected = game.getWayPoints(new Local("Local2"), new Local("Local5"))
                + game.getLocalPoints(new Local("Local5"))
                + game.getWayPoints(new Local("Local5"), new Local("Local24"))
                + game.getLocalPoints(new Local("Local24"));

        assertEquals(expected, result,0.001);
        assertTrue(path.size() == 1);

        // Testa que jogador nao consegue conquistar local
        assertFalse(game.playerCanConquer(new Player("Pers1"), new Local("Local20")));
    }

    /**
     * Exercicio 2
     * ALINEA B)
     */

    /**
     * Testa lista de aliancas de um determinado jogador passado por argumento
     * @throws Exception
     */
    @Test
    public void getAliances() throws Exception {
        // Testa aliados do Pers0
        List result = game.getAlliances(new Player("Pers0"));
        List expected = new ArrayList(Arrays.asList(new Player("Pers3"), new Player("Pers4")));
        assertEquals(expected, result);

        // Testa alidados do Pers3
        result = game.getAlliances(new Player("Pers3"));
        expected = new ArrayList(Arrays.asList(new Player("Pers0"), new Player("Pers1"), new Player("Pers4")));
        assertEquals(expected, result);
    }

    /**
     * Exercicio 2
     * ALINEA C)
     */

    /**
     * Testa retorno da alianca mais forte do jogo
     * @throws Exception
     */
    @Test
    public void strongestAliance() throws Exception {
        List l = new ArrayList();
        double result = game.strongestAlliance(l);
        double expected = 499.2;

        assertEquals(expected, result, 0.001);
        assertTrue(l.containsAll(new ArrayList(Arrays.asList(new Player("Pers0"), new Player("Pers3")))));
    }

    /**
     * Exercicio 2
     * ALINEA D)
     */

    /**
     * Testa criacao de nova alianca entre 2 jogadores
     * @throws Exception
     */
    @Test
    public void setAliance() throws Exception {
        BasicGraph net = game.getAlliancesNet();

        assertNotNull(net.getEdge(new Player("Pers0"), new Player("Pers4")));
        assertNotNull(net.getEdge(new Player("Pers0"), new Player("Pers1")));
        assertNotNull(net.getEdge(new Player("Pers9"), new Player("Pers8")));
        assertNull(net.getEdge(new Player("Pers1"), new Player("Pers7")));
        assertNull(net.getEdge(new Player("Pers2"), new Player("Pers6")));
        assertNull(net.getEdge(new Player("Pers9"), new Player("Pers4")));


        // Teste nova alianca dentro da mesma rede
        Graph graph = game.getAlliances();
        LinkedList path = new LinkedList();
        GraphAlgorithms.shortestPath(graph, new Player("Pers3"), new Player("Pers2"), path);

        double result = game.setAlliance(new Player("Pers3"), new Player("Pers2"), true);
        double expected = 0;
        for(int i = 0; i < path.size()-1; i++) {
            Player vOrig = (Player) path.get(i);
            Player vAdj = (Player) path.get(i+1);
            expected += graph.getEdge(vOrig, vAdj).getWeight();
        }
        expected = expected / (path.size() - 1);
        assertEquals(expected, result, 0.001);

        // Teste nova alianca dentro da mesma rede com alianca privada pelo meio
        GraphAlgorithms.shortestPath(graph, new Player("Pers5"), new Player("Pers6"), path);
        result = game.setAlliance(new Player("Pers5"), new Player("Pers6"), true);
        expected = 0;
        for(int i = 0; i < path.size()-1; i++) {
            Player vOrig = (Player) path.get(i);
            Player vAdj = (Player) path.get(i+1);
            expected += graph.getEdge(vOrig, vAdj).getWeight();
        }
        expected = (expected - 0.5) / (path.size() - 2);
        assertEquals(expected, result, 0.001);
    }

    /**
     * Exercicio 2
     * ALINEA E)
     */

    /**
     * Testa criacao de nova alianca entre 2 jogadores
     * @throws Exception
     */
    @Test
    public void setAllAliances() throws Exception {

        // Testa existencia de ramos entre todos os vertices
        Graph result = game.setAllAliances();
        for(Object p1 : game.getAlliances().vertices()) {
            for(Object p2 : game.getAlliances().vertices()) {
                if(!p1.equals(p2)) {
                    assertNotNull(result.getEdge(p1, p2));
                }
            }
        }
    }

    /**
     * Exercicio 3
     * ALINEA F)
     */

    /**
     * Testa conquista com outro jogador
     * @throws Exception
     */
    @Test
    public void conquerLocalWith() throws Exception {
        double[] pointsNeeded = new double[1];
        ArrayList list = new ArrayList();

        // Testa aliados a nao conseguirem conquistar local
        boolean result = game.conquerLocalWith(new Player("Pers1"), new Player("Pers2"), new Local("Local20"), pointsNeeded, list);
        assertFalse(result);

        // Testa aliados a conquistar local
        result = game.conquerLocalWith(new Player("Pers8"), new Player("Pers9"), new Local("Local4"), pointsNeeded, list);
        assertTrue(result);
    }

}