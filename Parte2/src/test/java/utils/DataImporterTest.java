package utils;

import controllers.GameProject;
import models.Local;
import models.Player;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

/**
 * Classe de teste unitarios de DataImporter
 */
public class DataImporterTest {
    String sTestLocalsFile, sTestPeersFile;
    String mTestLocalsFile, mTestPeersFile;
    String lTestLocalsFile, lTestPeersFile;
    String xlTestLocalsFile, xlTestPeersFile;

    /**
     * Set up da classe de teste
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        sTestLocalsFile = "./src/test/java/files/S/locais_S.txt";
        mTestLocalsFile = "files/M/locais_M.txt";
        lTestLocalsFile = "files/L/locais_L.txt";
        xlTestLocalsFile= "files/XL/locais_XL.txt";

        sTestPeersFile = "./src/test/java/files/S/pers_S.txt";
        mTestPeersFile = "files/M/pers_M.txt";
        lTestPeersFile = "files/L/pers_L.txt";
        xlTestPeersFile = "files/XL/pers_XL.txt";
    }

    /**
     * Exercicio 1
     * ALINEA A)
     */

    /**
     * Testa importacao de ficheiro locais_S.txt
     * @throws Exception
     */
    @Test
    public void importGameSLocalsFiles() throws Exception {
        GameProject game = new GameProject();
        DataImporter importer = new DataImporter();
        importer.importGame(sTestLocalsFile, game);

        // Verifica se todos os locais do ficheiro foram adicionados
        Set locals = game.getLocals();
        for(int i = 0; i < 30; i++) {
            assertTrue(locals.contains(new Local("Local"+i)));
        }

        // Verifica se pontos foram importados e mapeados
        assertTrue(game.getLocalPoints(new Local("Local0")) == 30);
        assertTrue(game.getLocalPoints(new Local("Local1")) == 29);
        assertTrue(game.getLocalPoints(new Local("Local2")) == 21);
        assertTrue(game.getLocalPoints(new Local("Local3")) == 23);
        assertTrue(game.getLocalPoints(new Local("Local4")) == 30);
        assertTrue(game.getLocalPoints(new Local("Local25")) == 30);
        assertTrue(game.getLocalPoints(new Local("Local26")) == 34);
        assertTrue(game.getLocalPoints(new Local("Local27")) == 38);
        assertTrue(game.getLocalPoints(new Local("Local28")) == 20);
        assertTrue(game.getLocalPoints(new Local("Local29")) == 31);

        //Verifica se caminhos foram importados e mapeados
        assertTrue(game.getWayPoints(new Local("Local0"), new Local("Local1")) == 29);
        assertTrue(game.getWayPoints(new Local("Local0"), new Local("Local3")) == 20);
        assertTrue(game.getWayPoints(new Local("Local0"), new Local("Local6")) == 26);
        assertTrue(game.getWayPoints(new Local("Local27"), new Local("Local28")) == 20);
        assertTrue(game.getWayPoints(new Local("Local28"), new Local("Local29")) == 22);

        //Lanca excepcao se caminho nao existe
        boolean expected = true;
        try {
            assertFalse(game.getWayPoints(new Local("Local29"), new Local("Local30")) == 23);
        } catch(NullPointerException e) {
            expected = false;
        }
        assertFalse(expected);

        //Lanca excepcao se local nao existe
        expected = true;
        try {
            assertFalse(game.getLocalPoints(new Local("Local30")) == 0);
        } catch(NullPointerException e) {
            expected = false;
        }
        assertFalse(expected);
    }


    /**
     * Exercicio 1
     * ALINEA A)
     */

    /**
     * Testa importacao de pers_S.txt
     * @throws Exception
     */
    @Test
    public void importGameSPlayersFiles() throws Exception {
        GameProject game = new GameProject();
        DataImporter importer = new DataImporter();
        importer.importGame(sTestPeersFile, game);

        Set players = game.getPlayers();

        // Verifica se todos os jogadores foram adicionados
        for(int i = 0; i < players.size(); i++) {
            assertTrue(players.contains(new Player("Pers"+i)));
        }

        // Verifica se pontos foram importados e mapeados
        assertTrue(game.getPlayerPoints(new Player("Pers0")) == 195);
        assertTrue(game.getPlayerPoints(new Player("Pers1")) == 111);
        assertTrue(game.getPlayerPoints(new Player("Pers2")) == 112);
        assertTrue(game.getPlayerPoints(new Player("Pers3")) == 429);
        assertTrue(game.getPlayerPoints(new Player("Pers4")) == 262);
        assertTrue(game.getPlayerPoints(new Player("Pers5")) == 126);
        assertTrue(game.getPlayerPoints(new Player("Pers6")) == 121);
        assertTrue(game.getPlayerPoints(new Player("Pers7")) == 354);
        assertTrue(game.getPlayerPoints(new Player("Pers8")) == 481);
        assertTrue(game.getPlayerPoints(new Player("Pers9")) == 463);

        //Verifica se caminhos foram importados e mapeados
        assertTrue(game.getAllianceStrength(new Player("Pers0"), new Player("Pers3")) == 0.8);
        assertTrue(game.getAllianceStrength(new Player("Pers0"), new Player("Pers4")) == 0.5);
        assertTrue(game.getAllianceStrength(new Player("Pers1"), new Player("Pers2")) == 0.5);
        assertTrue(game.getAllianceStrength(new Player("Pers7"), new Player("Pers8")) == 0.5);
        assertTrue(game.getAllianceStrength(new Player("Pers7"), new Player("Pers9")) == 0.5);
    }

}