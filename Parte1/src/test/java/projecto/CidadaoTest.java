package projecto;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Classe de testes a cidadao
 * @author Bruno Alves <1151242@isep.ipp.pt>
 * @author Paulo Sampaio <1040425@isep.ipp.pt>
 */
public class CidadaoTest {
    Cidadao c1, c2;

    @Before
    public void setUp() {
        c1 = new Cidadao("José1", "123456789", "ze1@mail.pt", "4000-325");
        c2 = new Cidadao("José2", "223456789", "ze2@mail.pt", "5000-325");
    }

    /**
     * Teste comparacao metodo equals
     */
    @Test
    public void testEquals() {
        Cidadao result = new Cidadao("José1", "123456789", "ze1@mail.pt", "4000-325");

        // Cidadao com os mesmo parametros
        assertEquals(c1, result);

        result.setNome(c2.getNome());
        result.setContribuinte(c2.getContribuinte());
        result.setEmail(c2.getEmail());
        result.setCodPostal(c2.getCodPostal());

        // Cidadao com parametros alterados por set
        assertNotEquals(result, c1);

        // Cidadao com parametros alterados por set para ficar igual a c2
        assertEquals(result, c2);
    }

    /**
     * Teste de hashcode igual a numero contribuinte que deve ser unico
     */
    @Test
    public void testHashCode() {
        assertEquals(Integer.parseInt(c1.getContribuinte()) ,c1.hashCode());
        assertNotEquals(Integer.parseInt(c2.getContribuinte()) ,c1.hashCode());

        assertEquals(Integer.parseInt(c2.getContribuinte()) ,c2.hashCode());
        assertNotEquals(Integer.parseInt(c1.getContribuinte()) ,c2.hashCode());
    }

    /**
     * Teste de compareTo pelo contribuinte
     */
    @Test
    public void testCompareTo() {
        assertTrue(c1.compareTo(c2) < 0);
        assertTrue(c1.compareTo(c1) == 0);
        assertTrue(c2.compareTo(c1) > 0);
    }

}