package projecto;

import org.junit.Test;
import projecto.utils.DoublyLinkedList;

import java.util.*;

import static org.junit.Assert.*;

public class ServicoPublicoTest {

    //Instância Servico Publico para todos os testes.
    private static ServicoPublico sp = new ServicoPublico();

    private static Reparticao r1 = new Reparticao("Porto", 1, "4000", new ArrayList<>());
    private static Reparticao r2 = new Reparticao("Braga", 2, "4700", new ArrayList<>());
    private static Reparticao r3 = new Reparticao("Viana do Castelo", 3, "4900", new ArrayList<>());
    private static Reparticao r4 = new Reparticao("Porto", 1, "4000", new ArrayList<>());

    private static Cidadao c1 = new Cidadao("José1", "123456789", "ze1@mail.pt", "4000-325");
    private static Cidadao c2 = new Cidadao("José2", "223456789", "ze2@mail.pt", "5000-325");
    private static Cidadao c3 = new Cidadao("José3", "323456789", "ze3@mail.pt", "6000-325");
    private static Cidadao c4 = new Cidadao("José4", "423456789", "ze4@mail.pt", "7000-325");
    private static Cidadao c5 = new Cidadao("José5", "523456789", "ze5@mail.pt", "2000-325");
    private static Cidadao c6 = new Cidadao("José6", "623456789", "ze6@mail.pt", "3250-325");
    private static Cidadao c7 = new Cidadao("José7", "723456789", "ze7@mail.pt", "4700-325");
    private static Cidadao c8 = new Cidadao("José8", "823456789", "ze8@mail.pt", "1300-325");

    private static Senha s1 = new Senha(1, "A");
    private static Senha s2 = new Senha(2, "A");
    private static Senha s3 = new Senha(1, "B");
    private static Senha s4 = new Senha(2, "B");
    private static Senha s5 = new Senha(3, "B");
    private static Senha s6 = new Senha(1, "C");


    /**
     * ALINEA B)
     * Teste para o método de adicionar nova Repartição
     * E para adicionar novos clientes a uma nova repartição adicionada
     * @throws Exception
     */
    @Test
    public void adicionaReparticao() throws Exception {

        sp = new ServicoPublico();

        System.out.println("Add Nova Reparticao");

        //Testes VERDADEIRO para adicionar nova repartição.
        assertTrue(sp.addReparticao(r1));
        assertTrue(sp.addReparticao(r2));
        assertTrue(sp.addReparticao(r3));

        Iterator it = sp.getReparticoes().iterator();

        //Teste FALSO para nova repartição.
        assertFalse(sp.addReparticao(r4));

        System.out.println("Add Cidadãos a Nova Repartição");

        sp.addCidadao(c1);
        sp.addCidadao(c2);
        sp.addCidadao(c3);
        sp.addCidadao(c4);

        //Teste para adicionar nova repartição e adicionar os clientes que têm o mesmo CP
        r4.setCodigoPostal("5000");
        r4.setNum(4);
        sp.addReparticao(r4);
        assertTrue(r4.getCidadaosAfectos().contains(c2));
    }

    /**
     * ALINEA C)
     * Teste para remover reparticaom
     * Verifica realocacao de cidadaos a nova reparticao
     */
    @Test
    public void removeReparticao() {
        System.out.println("Remove Reparticao");

        reiniciaSp();

        sp.removeReparticao(r1);
        assertTrue(r2.getCidadaosAfectos().contains(c1));

        sp.removeReparticao(r3);
        assertTrue(r2.getCidadaosAfectos().contains(c2));
        assertTrue(r2.getCidadaosAfectos().contains(c4));
    }

    /**
     * ALINEA D)
     * Testar estrutura para ter cidadaos por reparticao
     */
    @Test
    public void getCidadaosPorReparticaoTest() {
        System.out.println("Cidadaos Por Repartição");

        reiniciaSp();

        Map<Integer, String> expectedMap = new HashMap<>();
        expectedMap.put(r1.getNum(), c1.getContribuinte());
        expectedMap.put(r3.getNum(), c2.getContribuinte());
        expectedMap.put(r3.getNum(), c3.getContribuinte());
        expectedMap.put(r3.getNum(), c4.getContribuinte());

        assertEquals(expectedMap, sp.getCidadaosPorReparticao());

    }

    /**
     * ALINEA E)
     * Testar adicao de cidadao sem repeticoes
     */
    @Test
    public void addCidadaosTest() {
        System.out.println("Adicao de Cidadaos");
        reiniciaSp();

        Set expected = new HashSet();
        expected.add(c1);
        expected.add(c2);
        expected.add(c3);
        expected.add(c4);

        assertEquals(sp.getCidadaos(), expected);

        //Nao adiciona cidadao replicados
        expected.add(c1);
        assertEquals(sp.getCidadaos(), expected);

        //Nao adiciona conjunto cidadao replicados
        sp.addCidadaos(expected);
        assertEquals(sp.getCidadaos(), expected);

        //Adiciona conjunto, mesmo quando ainda nao existem reparticoes
        sp = new ServicoPublico();
        sp.addCidadaos(expected);
        assertEquals(sp.getCidadaos(), expected);
    }


    public void reiniciaSp() {

        sp = new ServicoPublico();

        sp.addReparticao(r1);
        sp.addReparticao(r2);
        sp.addReparticao(r3);

        sp.addCidadao(c1);
        sp.addCidadao(c2);
        sp.addCidadao(c3);
        sp.addCidadao(c4);

    }
}