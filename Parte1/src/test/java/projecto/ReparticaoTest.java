package projecto;

import edu.emory.mathcs.backport.java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import projecto.utils.DoublyLinkedList;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testes da classe Reparticao
 */
public class ReparticaoTest {
    private List listaServicos, listaCidadaos, listaSenhas;
    private Reparticao r1;
    private Cidadao c1, c2, c3, c4, c5, c6, c7, c8;
    private Senha s1, s2, s3, s4, s5, s6, s7;

    /**
     * Set up classe para teste
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        listaServicos = Arrays.asList(new String[]{"A", "B", "C"});
        r1 = new Reparticao("Porto", 1, "4000", listaServicos);

        c1 = new Cidadao("Bruno", "123456789", "baalves@isep.pt", "4100-012");
        c2 = new Cidadao("José2", "223456789", "ze2@mail.pt", "5000-325");
        c3 = new Cidadao("José3", "323456789", "ze3@mail.pt", "6000-325");
        c4 = new Cidadao("José4", "423456789", "ze4@mail.pt", "7000-325");
        c5 = new Cidadao("José5", "523456789", "ze5@mail.pt", "2000-325");
        c6 = new Cidadao("José6", "623456789", "ze6@mail.pt", "3250-325");
        c7 = new Cidadao("José7", "723456789", "ze7@mail.pt", "4700-325");
        c8 = new Cidadao("José8", "823456789", "ze8@mail.pt", "1300-325");

        s1 = new Senha(1, "A");
        s2 = new Senha(2, "A");
        s3 = new Senha(1, "B");
        s4 = new Senha(2, "B");
        s5 = new Senha(3, "B");
        s6 = new Senha(1, "C");
        s7 = new Senha(2, "C");

        listaCidadaos = Arrays.asList(new Cidadao[]{c1, c2, c3, c4, c5, c6, c7});
        listaSenhas = Arrays.asList(new Senha[]{s1, s2, s3, s4, s5, s6, s7});
    }

    /**
     * ALINEA F)
     * Testa adicao de cidadao a reparticao
     * @throws Exception
     */
    @Test
    public void addCidadaoAReparticaoTest() throws Exception {
        // Adicionar cidadao a reparticao
        boolean result = r1.addCidadao(c1);
        boolean expected = true;
        assertEquals(result, expected);

        // Nao aceitar duplicacao de cidadoes
        result = r1.addCidadao(c1);
        expected = false;
        assertEquals(result, expected);

        // Nao aceitar duplicacao de cidadoes
        Cidadao c2 = new Cidadao("Bruno", "123456789", "baalves@isep.pt", "4100-012");
        result = r1.addCidadao(c2);
        expected = false;
        assertEquals(result, expected);
    }

    /**
     * ALINEA F)
     * Testa atendimento de cidadao e reorganizacao das filas
     */
    @Test
    public void atenderCidadaoTest() {
        Reparticao instance = new Reparticao("Porto", 1, "4000", listaServicos);
        for(Object c : listaCidadaos)
            instance.addCidadao((Cidadao) c);

        s1.atribuiSenha(c2.getContribuinte());  // Cidadao c2 - Balcao A x-
        s3.atribuiSenha(c2.getContribuinte());  // Cidadao c2 - Balcao B -x
        s2.atribuiSenha(c3.getContribuinte());  // Cidadao c3 - Balcao A -x
        s4.atribuiSenha(c4.getContribuinte());  // Cidadao c4 - Balcao B x-
        s5.atribuiSenha(c5.getContribuinte());  // Cidadao c5 - Balcao B --x
        s6.atribuiSenha(c1.getContribuinte());  // Cidadao c4 - Balcao C x-
        s7.atribuiSenha(c2.getContribuinte());  // Cidadao c4 - Balcao C -x


        for(Object s : listaSenhas)
            instance.addSenha((Senha) s);

        instance.atenderCidadao();

        Map senhasEmEspera = instance.getSenhasEmEspera();
        DoublyLinkedList result = (DoublyLinkedList) senhasEmEspera.get("B");
        assertEquals(s3, result.first());


        instance.atenderCidadao();

        result = (DoublyLinkedList) senhasEmEspera.get("C");
        assertEquals(s7, result.first());
    }

    /**
     * ALINEA F)
     * Teste numero de atendimentos a uma determinada hora
     * @throws Exception lancada excepcao quando a hora e superior a da primeira senha por atender
     */
    @Test
    public void getNumAtendimentosTest() throws Exception {
        Reparticao instance = new Reparticao("Porto", 1, "4000", listaServicos);
        for(Object c : listaCidadaos)
            instance.addCidadao((Cidadao) c);

        s1.atribuiSenha(c2.getContribuinte());  // Cidadao c2 - Balcao A x-
        s3.atribuiSenha(c2.getContribuinte());  // Cidadao c2 - Balcao B -x
        s2.atribuiSenha(c3.getContribuinte());  // Cidadao c3 - Balcao A -x
        s4.atribuiSenha(c4.getContribuinte());  // Cidadao c4 - Balcao B x-
        s5.atribuiSenha(c5.getContribuinte());  // Cidadao c5 - Balcao B --x
        s6.atribuiSenha(c1.getContribuinte());  // Cidadao c4 - Balcao C x-
        s7.atribuiSenha(c2.getContribuinte());  // Cidadao c4 - Balcao C -x


        for(Object s : listaSenhas)
            instance.addSenha((Senha) s);

        instance.atenderCidadao();

        int result = instance.getNumAtendimentos("A", 0);
        assertEquals(0, result);

        result = instance.getNumAtendimentos("A", 10);
        assertEquals(1, result);

        instance.atenderCidadao();

        result = instance.getNumAtendimentos("A", 20);
        assertEquals(2, result);

        result = instance.getNumAtendimentos("A", 30);
        assertEquals(2, result);
    }

    /**
     * ALINEA G)
     * Testa servico mais procurado numa reparticao naquele dia
     */
    @Test
    public void servicosMaisProcuraTest() {
        Reparticao instance = new Reparticao("Porto", 1, "4000", listaServicos);
        for(Object c : listaCidadaos)
            instance.addCidadao((Cidadao) c);

        s1.atribuiSenha(c2.getContribuinte());  // Cidadao c2 - Balcao A x-
        s3.atribuiSenha(c2.getContribuinte());  // Cidadao c2 - Balcao B -x
        s2.atribuiSenha(c3.getContribuinte());  // Cidadao c3 - Balcao A -x
        s4.atribuiSenha(c4.getContribuinte());  // Cidadao c4 - Balcao B x-
        s5.atribuiSenha(c5.getContribuinte());  // Cidadao c5 - Balcao B --x
        s6.atribuiSenha(c1.getContribuinte());  // Cidadao c4 - Balcao C x-
        s7.atribuiSenha(c2.getContribuinte());  // Cidadao c4 - Balcao C -x


        for(Object s : listaSenhas)
            instance.addSenha((Senha) s);

        String result = instance.servicoMaisProcura();
        assertEquals("B", result);
    }

    /*
     * Outros metodos da classe
     */

    /**
     * Testa quando 2 objectos sao iguais
     * @throws Exception
     */
    @Test
    public void testEquals() throws Exception {
        // Reparticoes iguais
        Reparticao r1 = new Reparticao("Porto", 1, "4000", listaServicos);
        Reparticao r = new Reparticao("Porto", 1, "4000", listaServicos);
        assertEquals(r1, r);

        // Cidades diferentes
        r = new Reparticao("Lisboa", 2, "4000", listaServicos);
        assertNotEquals(r1, r);

        // Numero de reparticao diferente
        r = new Reparticao("Porto", 2, "4000", listaServicos);
        assertNotEquals(r1, r);

        // Codigo Postal diferente
        r = new Reparticao("Porto", 1, "4100", listaServicos);
        assertEquals(r1, r);

        // Lista de servicos diferente
        r = new Reparticao("Porto", 1, "4000", new ArrayList<>());
        assertEquals(r1, r);
    }

    /**
     * Testa hashcode a obter
     * @throws Exception
     */
    @Test
    public void testHashCode() throws Exception {
        // Verifica hashcode de 2 objectos iguais
        Reparticao r = new Reparticao("Porto", 1, "4000", listaServicos);
        int result = r1.hashCode();
        int expected = r.hashCode();

        assertEquals(result, expected);

        // Verifica hashcode 2 objectos diferenes
        r = new Reparticao("Porto", 0, "4100", listaServicos);
        expected = r.hashCode();
        assertNotEquals(result, expected);
    }
}