package projecto;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Classe de testes a senha
 * @author Bruno Alves <1151242@isep.ipp.pt>
 * @author Paulo Sampaio <1040425@isep.ipp.pt>
 */
public class SenhaTest {
    Senha s1, s2;

    @Before
    public void setUp() {
        s1 = new Senha(1, "A");
        s2 = new Senha(2, "A");
    }

    /**
     * Teste comparacao metodo equals
     */
    @Test
    public void testEquals() {
        Senha s = new Senha(1, "A");
        assertEquals(s, s1);
        assertNotEquals(s, s2);
    }

    /**
     * Teste de compareTo pelo contribuinte
     */
    @Test
    public void testCompareTo() {
        assertTrue(s1.compareTo(s2) < 0);
        assertTrue(s1.compareTo(s1) == 0);
        assertTrue(s2.compareTo(s1) > 0);
    }


}