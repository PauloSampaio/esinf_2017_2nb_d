package projecto.utils;

import org.junit.Before;
import org.junit.Test;
import projecto.Reparticao;
import projecto.ServicoPublico;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by brunofigalves on 07/10/17.
 */
public class UtilsTest {
    Utils u;
    String dadosReparticoesPath, dadosCidadaosPath, dadosSenhasPath;
    List cidades;

    @Before
    public void setUp() throws Exception {
        u = new Utils();
        dadosReparticoesPath = "./DadosReparticoes.csv";
        dadosCidadaosPath = "./DadosCidadaos.csv";
        dadosSenhasPath = "./DadosSenhas.csv";
        cidades = new ArrayList<>(Arrays.asList("PORTO", "LISBOA", "AVEIRO", "BEJA", "ÉVORA"
                ,"FARO", "BRAGANÇA", "BRAGA", "OVAR", "OEIRAS", "SETÚBAL", "ALBUFEIRA", "GONDOMAR"
                , "MATOSINHOS", "COIMBRA", "MEALHADA", "AVANCA", "OLIVEIRA DO BAIRRO", "ESPINHO"
                , "ESTORIL", "MAIA", "ALMADA", "CASCAIS", "CAMINHA", "VILA REAL", "VIANA DO CASTELO"
                , "GUIMARÃES", "FUNCHAL"));

    }

    /**
     * Teste para importacao de reparticoes por CSV
     * @throws Exception
     */
    @Test
    public void importarReparticoesCSV() throws Exception {
        ServicoPublico result = new ServicoPublico();
        u.importarReparticoesCSV(dadosReparticoesPath, result);

        /*Verificar tamanho da lista e nao importa duplicados*/
        assertEquals(28, result.getReparticoes().size());
        Iterator it = result.getReparticoes().iterator();

        /*Verificar se todas as cidades foram importadas*/
        while(it.hasNext()) {
            Reparticao r = (Reparticao)it.next();
            assertTrue(cidades.contains(r.getCidade()));
        }
    }

    /**
     * Teste de importacao de cidadaos por CSV
     * @throws Exception
     */
    @Test
    public void importarCidadaosCSV() throws Exception {
        ServicoPublico result = new ServicoPublico();
        u.importarReparticoesCSV(dadosReparticoesPath, result);
        u.importarCidadaosCSV(dadosCidadaosPath, result);
        assertEquals(174, result.getCidadaos().size());
    }

    @Test
    public void importarSenhasCSV() throws Exception {
        ServicoPublico result = new ServicoPublico();
        u.importarReparticoesCSV(dadosReparticoesPath, result);
        u.importarCidadaosCSV(dadosCidadaosPath, result);
        u.importarSenhasCSV(dadosSenhasPath, result);
    }

}