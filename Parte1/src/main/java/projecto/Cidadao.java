package projecto;

/**
 * Classe que representa um cidadao num servico publico
 * @author Bruno Alves <1151242@isep.ipp.pt>
 * @author Paulo Sampaio <1040425@isep.ipp.pt>
 */
public class Cidadao implements Comparable {

    private String nome;
    private String contribuinte;
    private String email;
    private String codPostal;

    /**
     * Construtor de cidadao
     * @param nome nome do cidadao
     * @param contribuinte numero fiscal
     * @param email email associado ao cidadao
     * @param codPostal codigo postal de residencia
     */
    public Cidadao(String nome, String contribuinte, String email, String codPostal) {
        this.nome = nome;
        this.contribuinte = contribuinte;
        this.email = email;
        this.codPostal = codPostal;
    }

    /**
     * Devolver nome
     * @return retorna uma string com nome do cidadao
     */
    public String getNome() {
        return nome;
    }

    /**
     * Definir nome
     * @param nome string com nome do cidadao a definir
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Devolver contribuinte
     * @return retorna string com contribuinte
     */
    public String getContribuinte() {
        return contribuinte;
    }

    /**
     * Definir contribuinte
     * @param contribuinte definir contribuinte do cidadao
     */
    public void setContribuinte(String contribuinte) {
        this.contribuinte = contribuinte;
    }

    /**
     * Devolver email
     * @return retorna stirng com email do cidadao
     */
    public String getEmail() {
        return email;
    }

    /**
     * Definir email
     * @param email string com email do cidadao a definir
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Devolver codigo postal
     * @return strinf com codigo postal do cidadao
     */
    public String getCodPostal() {
        return codPostal;
    }

    /**
     * Definir codigo postal
     * @param codPostal string com codigo postal a definir
     */
    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    /**
     * Verifica se objecto e igual a cidadao
     * @param o objecto para verificar igualdade
     * @return boolean com resultaod da igualdade
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cidadao)) return false;
        Cidadao that = (Cidadao) o;
        return this.contribuinte == that.contribuinte;
    }

    /**
     * Devolve hashcode para o objecto
     * @return retorna inteiro com hashcode do objecto
     */
    @Override
    public int hashCode() {
        return Integer.parseInt(this.contribuinte);
    }

    /**
     * Compara 2 cidadao pelo contribuinte para organizar numa estrutura de dados
     * @param o objecto cidadao a comparar
     * @return retorna inteiro com resultado da comparacao
     */
    @Override
    public int compareTo(Object o) {
        return Integer.parseInt(this.contribuinte)-Integer.parseInt(((Cidadao)o).getContribuinte());
    }
}
