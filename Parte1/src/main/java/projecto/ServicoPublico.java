package projecto;

import projecto.utils.DoublyLinkedList;

import java.util.*;

/**
 * Created by brunofigalves on 30/09/17.
 */
public class ServicoPublico {

    private DoublyLinkedList<Reparticao> reparticoes;
    private Set<Cidadao> cidadaos;

    /**
     * Construtor Serviço Publico
     */
    public ServicoPublico() {
        this.reparticoes = new DoublyLinkedList<>();
        this.cidadaos = new TreeSet<>();
    }

    /***********************************************************************************************/
    /*ALINEA B)*/

    /**
     * Adiciona uma noca repartição ao Servico Publico, se a mesma não existe
     * Realoca clientes ja existentes no caso de terem o mesmo codigo postal da nova reparticao
     * @param reparticao reparticao a adicionar
     * @return
     */
    public boolean addReparticao(Reparticao reparticao) {
        ListIterator<Reparticao> itr = reparticoes.listIterator();

        while(itr.hasNext()) {
            if(itr.next().equals(reparticao)) return false;
        }
        this.reparticoes.addFirst(reparticao);
        realocarCidadaos(reparticao);
        return true;
    }

    /**
     * Realoca cidadao a reparticao quando tem o memso codigo postal
     * @param reparticao reparticao para verificar realocacao
     */
    private void realocarCidadaos(Reparticao reparticao) {
        for (Cidadao c : this.cidadaos) {
            /*
             * verifica se os 4 primeiros digitos do CP do
             * cliente são iguais aos da nova reparticao
             * &&
             * verifica se esse cliente não existe já nessa reparticao
             */
            String cpCidadao = c.getCodPostal().substring(0,4);
            String cpReparticao = reparticao.getCodigoPostal().substring(0,4);
            if(cpCidadao.equals(cpReparticao)
                    && !reparticao.getCidadaosAfectos().contains(c)) {
                reparticao.addCidadao(c);
            }
        }
    }

    /***********************************************************************************************/
    /*ALINEA C)*/

    /**
     * Remove reparticao e distribuí os cidadãos afectos às repartições mais próximas
     * @return boolean
     */
    public boolean removeReparticao(Reparticao reparticao) {

        ListIterator<Reparticao> itr = reparticoes.listIterator();

        while (itr.hasNext()) {

            Reparticao rAtual = itr.next();


            if(rAtual.equals(reparticao)) {

                itr.remove();

                    addCidadaos(reparticao.getCidadaosAfectos());
                    rAtual.getCidadaosAfectos().clear();

                return true;
            }
        }
        return false;

    }
    /***********************************************************************************************/
    /*ALINEA D)*/

    /**
     * Obter numero da reparticao e cidade, por cidadao afectos
     */
    public Map<Integer, String> getCidadaosPorReparticao() {

        Map<Integer, String> map = new HashMap<>();

        ListIterator<Reparticao> itr = reparticoes.listIterator();

        while(itr.hasNext()) {
            Reparticao r = itr.next();

            for (Cidadao c : r.getCidadaosAfectos()) {

                map.put(r.getNum(), c.getContribuinte());
            }
        }
        return map;
    }
    /***********************************************************************************************/
    /*ALINEA E)*/

    /**
     * Adicionar conjunto de cidadaos
     * @param sCidadaos set de cidadaos
     */
    public void addCidadaos(Set<Cidadao> sCidadaos) {
        for(Cidadao c : sCidadaos) {
            addCidadao(c);
        }
    }

    /**
     * Adiciona cidadao ao Servico Publico
     * @param c cidadao a adicionar
     */
    public void addCidadao(Cidadao c) {
        Iterator it = reparticoes.iterator();
        Reparticao repAssociar = null;
        int min = Integer.MAX_VALUE;
        int cpCidadao = Integer.parseInt(c.getCodPostal().substring(0, 4));

        while(it.hasNext()) {
            Reparticao r = (Reparticao) it.next();
            int cpReparticao = Integer.parseInt(r.getCodigoPostal());
            int abs = Math.abs(cpCidadao - cpReparticao);
            if(abs < min) {
                repAssociar = r;
                min = abs;
            }
        }

        this.cidadaos.add(c);
        Reparticao r = this.getReparticao(c);
        if(r != null) {
            r.removeCidadao(c);
        }
        if(reparticoes.size() > 0)
            repAssociar.addCidadao(c);
    }

    /***********************************************************************************************/
    /* ALINEA G)*/

    /**
     * Permite saber o servico mais procurado numa reparticao
     * @param r reparticao a procurar
     * @return retorna string com servico
     */
    public String getServicoMaiorProcura(Reparticao r) {
        return r.servicoMaisProcura();
    }

    /***********************************************************************************************/
    /**
     * Alinea h)
     */
    public void sairFila(Reparticao r , Cidadao c) {
        r.cidadaoAbandonaFila(c);
    }

    /**
     * Retorna lista de reparticoes
     * @return retorna DoublyLinkedList com reparticoes
     */
    public DoublyLinkedList getReparticoes() {
        return this.reparticoes;
    }

    /***********************************************************************************************/

    /*Auxiliares*/
    /**
     * Retorna cidadaos do Servico Publico
     * @return conjunto de cidadaos
     */
    public Set getCidadaos() {
        return this.cidadaos;
    }

    /**
     * Calcula a diferença entre o codigo postal de um cliente e duas reparticoes
     * devolvendo a reparticao mais proxima do codigo postal do cliente
     * @param c
     * @param rPrev
     * @param rNext
     * @return
     */
    public Reparticao diferencaCps (Cidadao c, Reparticao rPrev, Reparticao rNext) {

        int cpPrev = Integer.parseInt(rPrev.getCodigoPostal().substring(0, 4));
        int cpNext = Integer.parseInt(rNext.getCodigoPostal().substring(0, 4));
        int cpCidadao = Integer.parseInt(c.getCodPostal().substring(0, 4));

        if(Math.abs(cpPrev-cpCidadao) < Math.abs(cpNext-cpCidadao)) {
            return rPrev;
        } else {
            return rNext;
        }
    }

    /**
     * Verifica se cidadao tem reparticao atribuida
     * @param c
     * @return
     */
    private Reparticao getReparticao(Cidadao c) {
        Iterator<Reparticao> it = reparticoes.iterator();
        Reparticao r = null;
        while(it.hasNext()) {
            r = it.next();
            if(r.getCidadaosAfectos().contains(c)) break;
        }
        return r;
    }
}
