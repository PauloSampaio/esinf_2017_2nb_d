package projecto.utils;

import java.io.*;
import java.util.*;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import projecto.Cidadao;
import projecto.Reparticao;
import projecto.Senha;
import projecto.ServicoPublico;

/**
 * Created by brunofigalves on 29/09/17.
 */
public class Utils {

    private static final char DELIMITER = ';';
    private static final String ENCODING = "UTF-8";

    /**
     * Le de ficheiro csv reparticoes e carregar num servico publico
     * @param directorio local do ficheiro csv
     * @param sPublico servico publico que controla reparticoes, cidadaos e senhas
     * @throws IOException lanca excepcao quando ha erro de leitura no ficheiro csv
     */
    public void importarReparticoesCSV(String directorio, ServicoPublico sPublico) throws IOException {
        File fx = new File(directorio);
        if (!fx.exists()) {
            throw new IOException();
        }
        try (
            FileInputStream fis = new FileInputStream(fx);
            Reader reader = new InputStreamReader(fis, ENCODING)) {
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withDelimiter(DELIMITER));

            boolean primeiroRecord = true;
            for (CSVRecord record : parser) {
                String cidade = record.get(0);
                if(primeiroRecord) {                //primeiro caracter e BOM
                    cidade = cidade.substring(1);
                    primeiroRecord = false;
                }
                int num = Integer.parseInt(record.get(1));
                String codPostal = record.get(2);
                List<String> servicos = new ArrayList<>();
                for (int i = 3; i < record.size(); i++) {
                    servicos.add(record.get(i));
                }
                /*
                 * Alinea b): "Não podem existir repartições repetidas,
                 * nem duplicadas no mesmo código postal
                 * (primeiros quatro algarismos)"
                 */
                Reparticao r = new Reparticao(cidade, num, codPostal, servicos);
                sPublico.addReparticao(r);
            }
        }
    }

    /**
     * Le de ficheiro csv cidadoes e carregar num servico publico
     * @param directorio local do ficheiro csv
     * @param sPublico servico publico que controla reparticoes, cidadaos e senhas
     * @throws IOException lanca excepcao quando ha erro de leitura no ficheiro csv
     */
    public void importarCidadaosCSV(String directorio, ServicoPublico sPublico) throws IOException {
        File fx = new File(directorio);
        if (!fx.exists()) {
            throw new IOException();
        }
        try (
            FileInputStream fis = new FileInputStream(fx);
            Reader reader = new InputStreamReader(fis, ENCODING)) {
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withDelimiter(DELIMITER));

            boolean primeiroRecord = true;
            for(CSVRecord record : parser) {
                String nome = record.get(0);
                if(primeiroRecord) {                //primeiro caracter e BOM
                    nome = nome.substring(1);
                    primeiroRecord = false;
                }
                String contribuinte = record.get(1);
                String email = record.get(2);
                String codPostal = record.get(3);
                Cidadao c = new Cidadao(nome, contribuinte, email, codPostal);
                /*
                 * Alinea b): "Todos os cidadãos cujo código postal
                 * seja igual ao da nova repartição devem ser “passados” para ela.
                 * Notar que quando os dados são lidos dos ficheiros,
                 * o código postal do cidadão poderá não ser igual ao
                 * da repartição a que está afecto.
                 */
                sPublico.addCidadao(c);
            }
        }
    }

    /**
     * Le de ficheiro csv cidadoes e carrega no servico publico
     * @param directorio local do ficheiro csv
     * @param sPublico servico publico que controla reparticoes, cidadaos e senhas
     * @throws IOException lanca excepcao quando ha erro de leitura no ficheiro csv
     */
    public void importarSenhasCSV(String directorio, ServicoPublico sPublico) throws IOException {
        File fx = new File(directorio);
        if (!fx.exists()) {
            throw new IOException();
        }
        try (
            FileInputStream fis = new FileInputStream(fx);
            Reader reader = new InputStreamReader(fis, ENCODING)) {
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withDelimiter(DELIMITER));
            boolean primeiroRecord = true;
            for(CSVRecord record : parser) {
                String contribuinte = record.get(0);
                if(primeiroRecord) {                //primeiro caracter e BOM
                    contribuinte = contribuinte.substring(1);
                    primeiroRecord = false;
                }
                String codAssunto = record.get(1);
                int nOrdem = Integer.parseInt(record.get(2));
                Senha s = new Senha(nOrdem, codAssunto);
                s.atribuiSenha(contribuinte);
                Iterator it = sPublico.getReparticoes().iterator();
                while(it.hasNext()) {
                    Reparticao r = (Reparticao) it.next();
                    for(Cidadao c : r.getCidadaosAfectos()) {
                        if(c.getContribuinte().equals(contribuinte)) {
                            r.addSenha(s);
                        }
                    }
                }
            }
        }
    }


}
