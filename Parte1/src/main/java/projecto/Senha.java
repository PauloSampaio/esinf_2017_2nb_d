package projecto;

import java.util.Objects;

/**
 * Classe que representa uma senha de uma reparticao
 * @author Bruno Alves <1151242@isep.ipp.pt>
 * @author Paulo Sampaio <1040425@isep.ipp.pt>
 */
public class Senha implements Comparable{
    private int numOrdem;
    private String servico;
    private int horaAtendimento;
    private String contribuinteCidadao;

    /**
     * Construtor de senha
     * @param numOrdem numero de ordem
     */
    public Senha(int numOrdem, String servico) {
        this.numOrdem = numOrdem;
        this.servico = servico;
        this.contribuinteCidadao = null;
        this.horaAtendimento = 0;
    }

    /**
     * Atribui senha ao respetivo cidadao pelo contribuinte
     * @param contribuinte contribuinte de cidadao a quem vai ser atribuida senha
     */
    public void atribuiSenha (String contribuinte) {
        this.contribuinteCidadao = contribuinte;
    }

    /**
     * Devolve o contribuinte do cidadao que tirou a senha
     * @return string com contribuinte
     */
    public String getContribuinte() { return this.contribuinteCidadao; }

    /**
     * Definir numero de ordem
     * @param numOrdem numero de ordem
     */
    public void setNumOrdem(int numOrdem) {
        this.numOrdem = numOrdem;
    }

    /**
     * Devolver numero de ordem
     * @return numero de ordem
     */
    public int getNumOrdem() {
        return numOrdem;
    }

    /**
     * Devolve o serviço da senha
     * @return string com serivco da senha
     */
    public String getServico() {
        return this.servico;
    }

    /**
     * Define a hora prevista de atendimento de uma determinada senha
     * @param minutos
     */
    public void setHoraAtendimento(int minutos) {
        this.horaAtendimento = minutos;
    }

    /**
     * Devolve a hora de atendimento
     * @return
     */
    public int getHoraAtendimento() {
        return this.horaAtendimento;
    }

    /**
     * Verificar se objecto sao iguais
     * @param o objecto a verificar igualdade
     * @return retorna se e igual
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Senha)) return false;
        Senha senha = (Senha) o;
        return numOrdem == senha.numOrdem &&
                servico == senha.servico;
    }

    /**
     * Devolve hashcode para o objecto
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(numOrdem);
    }

    /**
     * Compara 2 senhas pelo numero de ordem para organizar numa estrutura de dados
     * @param o objecto senha a comparar
     * @return retorna inteiro com resultado da comparacao
     */
    @Override
    public int compareTo(Object o) {
        Senha s = (Senha) o;
        return this.getNumOrdem() - (s.getNumOrdem());
    }
}
