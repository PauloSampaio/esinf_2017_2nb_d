package projecto;

import projecto.utils.DoublyLinkedList;

import java.util.*;

/**
 * Reparticao de financas
 */
public class Reparticao implements Comparable {

    private String cidade;
    private int num;
    private String codigoPostal;
    private List<String> servicos;
    private Set<Cidadao> cidadaosAfectos;
    private Map<String, DoublyLinkedList<Senha>> senhasEmEspera;
    public Map<String, DoublyLinkedList<Senha>> senhasAtendidas;

    /**
     * Construtor de reparticao
     * @param cidade cidade onde se localiza reparticao
     * @param num numero da reparticao
     * @param codigoPostal codigo postal da reparticao
     * @param servicos servicos disponibilizados pela reparticao
     */
    public Reparticao(String cidade, int num, String codigoPostal, List<String> servicos) {
        this.cidade = cidade;
        this.num = num;
        this.codigoPostal = codigoPostal;
        this.servicos = servicos;
        this.cidadaosAfectos = new HashSet<Cidadao>();
        this.senhasEmEspera =  new TreeMap<>();
        this.senhasAtendidas =  new TreeMap<>();


        for(String servico : servicos) {
            if(!servico.isEmpty()) {
                this.senhasEmEspera.put(servico, new DoublyLinkedList<>());
                this.senhasAtendidas.put(servico, new DoublyLinkedList<>());
            }
        }
    }

    /**
     * Obter cidade da reparticao
     * @return retorna String com cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Definir cidade onde se localiza reparticao
     * @param cidade cidade a definir
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * Obter numero da reparticao
     * @return retorna numero da reparticao
     */
    public int getNum() {
        return num;
    }

    /**
     * Obter senhas Por Repartição
     * @return
     */
    public Map<String, DoublyLinkedList<Senha>> getSenhasEmEspera() {
        return this.senhasEmEspera;
    }

    /**
     * Obter senhas Atendidas por servico
     * @return
     */
    public Map<String, DoublyLinkedList<Senha>> getSenhasAtendidas() {
        return this.senhasAtendidas;
    }

    /**
     * Definir numero da reparticao
     * @param num numero a definir
     */
    public void setNum(int num) {
        this.num = num;
    }

    /**
     * Obter numero da reparticao
     * @return retorna o codigo postal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * Definir codigo postal
     * @param codigoPostal codigo postal a definir
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * Obter codigo postal
     * @return lista de servicos
     */
    public List getServicos() {
        return servicos;
    }

    /**
     * Define lista de servicos da reparticao
     * @param servicos lista a definir de servicos
     */
    public void setServicos(List servicos) {
        this.servicos = servicos;
    }

    /**
     * Obter conjunto de cidadoes afectos a reparticao
     * @return retorna conjunto de cidadoes afectos a reparticao
     */
    public Set<Cidadao> getCidadaosAfectos() {
        return this.cidadaosAfectos;
    }

    /**
     * Afectar cidadao a reparticao
     * @param c cidadao a adicionar
     * @return retorna se adicao foi bem sucedida
     */
    public boolean addCidadao(Cidadao c) {
        return cidadaosAfectos.add(c);
    }

    /**
     * Remove um cidado da Reparticao
     * @param c
     * @return
     */
    public boolean removeCidadao(Cidadao c) { return this.cidadaosAfectos.remove(c);}

    /**
     * Atribuí hora de atendimento a um senha de acordo com número da ordem
     */
    public void addHoraAtendimento(Senha senha) {
        senha.setHoraAtendimento(this.senhasEmEspera.get(senha.getServico()).size() * 10);
    }

    public void addSenha (Senha s) {
        this.addHoraAtendimento(s);
        this.senhasEmEspera.get(s.getServico()).addLast(s);
    }

    /**
     * Verifica igualdade com objecto passado por argumento se coddigo postal e numero forem iguais.
     * @param o objecto a comparar
     * @return retorna resultado da igualdade
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reparticao that = (Reparticao) o;

        return num == that.num;
    }

    /**
     * Funcao de hash a retornar o codigo postal.
     * Nao havera mais que uma reparticao por codigo postal.
     * @return retorna codigo postal para mapeamento na tabela de hash
     */
    @Override
    public int hashCode() {
        return Integer.parseInt(this.codigoPostal);
    }

    /**
     * Compara 2 reparticoes pelo codigo postal
     * @param o objecto a comparar
     * @return
     */
    public int compareTo(Object o) {
        int oCodPostal = Integer.parseInt(((Reparticao)o).getCodigoPostal());
        return Integer.parseInt(this.codigoPostal) - oCodPostal;
    }


    /***********************************************************************************************/
    /**
     * Alinea f)
     */

    /**
     * Atender cidadao em lista de espera
     */
    public void atenderCidadao() {
        Set<String> aAtender = new HashSet<>();
        for(String servico : this.servicos) {
            if(senhasEmEspera.get(servico).isEmpty()) continue;
            Senha s = senhasEmEspera.get(servico).first();
            boolean flag = aAtender.add(s.getContribuinte());
            if(flag) {
                senhasEmEspera.get(servico).removeFirst();
                senhasAtendidas.get(servico).addFirst(s);
            }
            else {
                DoublyLinkedList lista = senhasEmEspera.get(servico);
                Senha s1 = (Senha) lista.removeFirst();
                Senha s2 = (Senha) lista.removeFirst();
                lista.addFirst(s1);
                int aux = s1.getHoraAtendimento();
                if(s2 == null) {
                    s1.setHoraAtendimento(s1.getHoraAtendimento()+10);
                }
                else {
                    senhasAtendidas.get(servico).addFirst(s2);
                    s1.setHoraAtendimento(s2.getHoraAtendimento());
                    s2.setHoraAtendimento(aux);
                }
            }
        }
    }

    /**
     * Obter numero de atendimentos
     * @return retorna inteiro com numero de atendimentos
     */
    public int getNumAtendimentos(String servico, int minutos) throws Exception {
        int atendimentos = 0;

        Iterator it = senhasAtendidas.get(servico).iterator();

        while(it.hasNext()) {
            Senha s = (Senha) it.next();
            if(senhasEmEspera.get(servico).size() != 0
                    && senhasEmEspera.get(servico).first().getHoraAtendimento() < minutos) {
                throw new Exception("Hora superior a primeira senha em espera.");
            }
            if(s.getHoraAtendimento() < minutos) atendimentos++;
        }

        return atendimentos;
    }


    /***********************************************************************************************/
    /**
     * Alinea g)
     * Determina servico(s) com mais procura
     * @return
     */
    public String servicoMaisProcura() {
        int max = Integer.MIN_VALUE;
        String result = null;

        for(String servico : servicos) {
            if((senhasEmEspera.get(servico).size()+senhasAtendidas.get(servico).size()) > max) {
                max = senhasEmEspera.get(servico).size()+senhasAtendidas.get(servico).size();
                result = servico;
            }
        }
        return result;
    }

    /***********************************************************************************************/
    /**
     * Alinea h)
     * Organiza fila de senhas depois de Cidadao desistir
     * @param c
     */
    public void cidadaoAbandonaFila(Cidadao c) {

        for(String servico : servicos) {

            Iterator itr = this.senhasEmEspera.get(servico).iterator();

            boolean flag = false;

            while(itr.hasNext()) {
                Senha s = (Senha) itr.next();

                if(s.getContribuinte().equalsIgnoreCase(c.getContribuinte()) && !flag) {
                    flag = true;
                    itr.remove();
                    //Se itr.next falhar nos testes a verificar:
                    //itr.previous();
                    continue;
                } else if(flag) {
                    s.setHoraAtendimento(s.getHoraAtendimento()-10);
                }
            }
        }
    }
}
