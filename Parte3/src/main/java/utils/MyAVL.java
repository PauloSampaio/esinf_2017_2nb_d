package utils;

import models.Prefix;

/**
 * Classe que implementa e adiciona alguns metodos a AVL
 *
 * @param <E> Implementada com elementos comparable
 */
public class MyAVL<E extends Comparable<E>> extends AVL {
    /**
     * Procura na AVL no com numero de lados iguais ao valor passado por argumento
     *
     * @param sides numero de lados
     * @return retorna string com nome
     */
    public String find(int sides) {
        Node<Prefix> n = find(new Prefix<Integer, String>(sides, null), root);
        return (String) n.getElement().getValue();
    }

    /**
     * Procura na AVL numero de lados com nome igual ao passado por argumento
     *
     * @param name nome do poligono
     * @return retorna inteiro com numero d elados
     */
    public int find(String name) {
        Node<Prefix> n = find(new Prefix(name, 0), root);
        return (int) n.getElement().getValue();
    }

    /**
     * Metodo recursivo para percorrer uma AVL e encontrar um elemento
     * Complexidade O(log(n))
     * @param element elemento a encontrar na AVL
     * @param node    no de partida para procurar
     * @return retorna no de resultado
     */
    protected Node<Prefix> find(Prefix element, Node<Prefix> node) {
        if (node == null) return null;
        // Alteracao face ao metodo da BST
        if (node.getElement().compareTo(element) == 0) return node;
        if (node.getElement().compareTo(element) > 0) {
            return find(element, node.getLeft());
        } else {
            return find(element, node.getRight());
        }
    }

    /**
     * Procura o no 'pai' comum aos dois elementos passados por argumento na AVL
     *
     * @param p elemento 1
     * @param q elemento 2
     * @return retorna elemento de no encontrado
     */
    public E lowestCommonAncestor(Prefix p, Prefix q) {
        return lowestCommonAncestor(root, p, q).getElement();
    }

    /**
     * Metodo privado para procurar na AVL o LCA
     * Complexidade O(log(n))
     *
     * @param root no a partir do qual ira ser feita a pesquisa
     * @param p    elemento 1
     * @param q    elemento 2
     * @return retorna no encontrado
     */
    private Node<E> lowestCommonAncestor(Node root, Prefix p, Prefix q) {
        if (root == null)
            return null;

        if (root.getElement().equals(p) || root.getElement().equals(q))
            return root;

        Node l = lowestCommonAncestor(root.getLeft(), p, q);
        Node r = lowestCommonAncestor(root.getRight(), p, q);

        if (l != null && r != null) {
            return root;
        } else if (l == null && r == null) {
            return null;
        } else {
            return l == null ? r : l;
        }
    }
}
