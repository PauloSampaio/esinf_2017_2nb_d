package utils;

import controllers.PolygonsCtrl;
import models.Prefix;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

/**
 * Classe para importar dados para o controller
 */
public class DataImporter {
    private static final char DELIMITER = ';';
    private static final String ENCODING = "UTF-8";

    /******************************************************************************************************************
     * ALINEA A)
     ******************************************************************************************************************/

    /**
     * Importa prefixo de ficheiro para controller
     *
     * @param path path do ficheiro a importar
     * @param ctrl controller a carregar com dados
     * @throws IOException execpcao lancada com erro IO
     */
    public void importPrefixes(String path, PolygonsCtrl ctrl) throws IOException {
        File fx = new File(path);
        if (!fx.exists()) {
            throw new IOException();
        }
        try (
                FileInputStream fis = new FileInputStream(fx);
                Reader reader = new InputStreamReader(fis, ENCODING)) {
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withDelimiter(DELIMITER));

            boolean firstRecord = true;
            for (CSVRecord record : parser) {
                ctrl.addPrefix(new Prefix(Integer.parseInt(record.get(0)), record.get(1)));
            }
        }
    }

    /******************************************************************************************************************
     * ALINEA B)
     ******************************************************************************************************************/

    /**
     * Retorna map com mapeamento de ficheiro de testes. Numero lado por nome
     *
     * @param path path do ficheiro de testes
     * @return retorna map com mapeamento de ficheiro de testes
     * @throws IOException
     */
    public Map<Integer, String> importTestFileSidesNames(String path) throws IOException {
        Map<Integer, String> result = new TreeMap<>();
        File fx = new File(path);
        if (!fx.exists()) {
            throw new IOException();
        }
        try (
                FileInputStream fis = new FileInputStream(fx);
                Reader reader = new InputStreamReader(fis, ENCODING)) {
            final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withDelimiter(DELIMITER));

            boolean firstRecord = true;
            for (CSVRecord record : parser) {
                result.put(Integer.parseInt(record.get(0)), record.get(1));
            }
        }

        return result;
    }

}
