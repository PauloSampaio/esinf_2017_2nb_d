package controllers;

import models.Prefix;
import utils.MyAVL;

import java.util.LinkedList;
import java.util.List;

/**
 * Classe controller de poligonos
 */
public class PolygonsCtrl {
    private MyAVL units, dozens, hundreds;

    /**
     * Construtor de controller de poligonos
     */
    public PolygonsCtrl() {
        units = new MyAVL();
        dozens = new MyAVL();
        hundreds = new MyAVL();
    }

    /******************************************************************************************************************
     * ALINEA A)
     ******************************************************************************************************************/

    /**
     * Adiciona novo prefixo as AVL(s)
     * Complexidade O(log(n))
     *
     * @param p prefixo a adicionar
     */
    public void addPrefix(Prefix p) {
        int s = (int) p.getKey();
        if (s >= 1 && s <= 9)
            units.insert(p);
        else if (s >= 10 && s <= 99)
            dozens.insert(p);
        else if (s >= 100 && s <= 900)
            hundreds.insert(p);
    }

    /**
     * Verifica se AVL(s) contem argumento passado como prefixo
     * Complexidade O(n*log(n))
     *
     * @param p prefixo a verificar se ja existes nas AVL(s)
     * @return retorna se ja existe nas estruturas
     */
    public boolean contains(Prefix p) {
        int s = (int) p.getKey();
        if (s >= 1 && s <= 9)
            for (Object o : units.inOrder()) {
                if (o.equals(p)) return true;
            }
        else if (s >= 10 && s <= 99)
            for (Object o : dozens.inOrder()) {
                if (o.equals(p)) return true;
            }
        else if (s >= 100 && s <= 900)
            for (Object o : hundreds.inOrder()) {
                if (o.equals(p)) return true;
            }
        return false;
    }

    /******************************************************************************************************************
     * ALINEA B)
     ******************************************************************************************************************/

    /**
     * Obter o nome de um poligono para um numero de lados passado por argumento
     * Complexidade O(log(n))
     *
     * @param sides numero de lados do poligono
     * @return retorna string com nome do poligono
     */
    public String getPolygonName(int sides) {
        String result = "";
        int[] arr = new int[3];
        for (int i = 0; i < 3; i++) {
            arr[i] = sides % 10;
            sides = sides / 10;
        }

        // Procura na arvore das centenas
        if (arr[2] != 0) {
            result += hundreds.find(arr[2] * 100);
        }

        // Procura na arvore das dezenas
        boolean unitsFlag = true;
        if (arr[1] != 0) {
            if (arr[1] * 10 > 29) {
                result += dozens.find(arr[1] * 10);
            } else {
                result += dozens.find(arr[1] * 10 + arr[0]);
                unitsFlag = false;
            }
        }

        // Procura na arvore das unidades
        if (unitsFlag && arr[0] != 0) {
            result += units.find(arr[0]);
        }

        return result + "gon";
    }

    /******************************************************************************************************************
     * ALINEA C)
     ******************************************************************************************************************/

    /**
     * Devolve AVL com todos os nomes de poligonos com lados entre 1~999
     * Complexidade O((log(n))^2))
     *
     * @return retorna AVL com nomes de poligonos
     */
    public MyAVL getPolygonTree() {
        MyAVL<String> result = new MyAVL();
        for (int i = 1; i < 1000; i++) {
            result.insert(new Prefix(getPolygonName(i), i));
        }
        return result;
    }

    /******************************************************************************************************************
     * ALINEA D)
     ******************************************************************************************************************/

    /**
     * Devolve numero de lados para um determinado nome de poligono passado por argumento
     * Complexidade O(log(n))
     *
     * @param p nome do poligono
     * @return retorna inteiro com nome de lados
     */
    public int getSidesPolygon(String p) {
        return getPolygonTree().find(p);
    }

    /******************************************************************************************************************
     * ALINEA E)
     ******************************************************************************************************************/

    /**
     * Devolve lista com nome de poligonos no intervalo
     * Complexidade O(n*log(n))
     *
     * @param start inicio do intervalo
     * @param end   fim do intervalo
     * @return retorna lista com nomes
     */
    public List getNamesPolygon(int start, int end) {
        if (start < 1 || end > 999 || start > end) {
            throw new IllegalArgumentException();
        }
        LinkedList<String> result = new LinkedList();
        for (int i = start; i < end + 1; i++) {
            result.push(getPolygonName(i));
        }
        return result;
    }

    /******************************************************************************************************************
     * ALINEA F)
     ******************************************************************************************************************/

    /**
     * Procura o no 'pai' comum aos dois elementos passados por argumento na AVL
     * Complexidade O(log(n))
     *
     * @param p elemento 1
     * @param q elemento 2
     * @return retorna elemento de no encontrado
     */
    public Prefix findLCA(String p, String q) {
        return (Prefix) getPolygonTree().lowestCommonAncestor(new Prefix(p, getSidesPolygon(p)), new Prefix(q, getSidesPolygon(q)));
    }

}
