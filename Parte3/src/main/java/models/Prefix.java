package models;

import java.util.Objects;

/**
 * Classe que define prefixo de poligono
 */
public class Prefix<K extends Comparable, V> implements Comparable<Prefix> {
    private K key;
    private V value;

    /**
     * Contrutor de prefixo de poligonos
     *
     * @param key   valor chave
     * @param value valor associado a chave
     */
    public Prefix(K key, V value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Retorna valor chave
     *
     * @return retorna objeto K
     */
    public K getKey() {
        return key;
    }

    /**
     * Retorna valor associado a chave
     *
     * @return retorna objeto  V
     */
    public V getValue() {
        return value;
    }

    /**
     * Compara prefixo com outro prefix
     *
     * @param o objeto prefixo a comparar
     * @return retorna comparacao pelo valor chave
     */
    @Override
    public int compareTo(Prefix o) {
        return key.compareTo(o.getKey());
    }

    /**
     * Verifica se dois objectos sao iguais
     *
     * @param o objecto a comparar
     * @return retorna se objectos sao iguais
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Prefix)) return false;
        Prefix prefix = (Prefix) o;
        return key.equals(prefix.getKey()) &&
                value.equals(prefix.getValue());
    }

    /**
     * Hashcode para objecto
     *
     * @return retorna numero de hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

}
