package controllers;

import models.Prefix;
import org.junit.Before;
import org.junit.Test;
import utils.DataImporter;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Classe de testes de controller PolygonsCtrl
 */
public class PolygonsCtrlTest {
    private DataImporter importer;
    private PolygonsCtrl ctrl;
    private String pathUnitsFile, pathDozensFiles, pathHundredsFile;
    private String pathTestFile1;

    /**
     * Setup classe de testes
     *
     * @throws IOException erro IO na leitura de fecheiros
     */
    @Before
    public void setUp() throws IOException {
        importer = new DataImporter();
        ctrl = new PolygonsCtrl();
        pathUnitsFile = "src/test/files/poligonos_prefixo_unidades.txt";
        pathDozensFiles = "src/test/files/poligonos_prefixo_dezenas.txt";
        pathHundredsFile = "src/test/files/poligonos_prefixo_centenas.txt";
        pathTestFile1 = "src/test/files/teste_lados_nome.txt";


        importer.importPrefixes(pathUnitsFile, ctrl);
        importer.importPrefixes(pathDozensFiles, ctrl);
        importer.importPrefixes(pathHundredsFile, ctrl);
    }

    /******************************************************************************************************************
     * ALINEA B)
     ******************************************************************************************************************/

    /**
     * Testa retorno de nome de poligono
     *
     * @throws IOException lanca excepcao quando ha erro IO
     */
    @Test
    public void getPolygonName() throws IOException {
        Map<Integer, String> instance = importer.importTestFileSidesNames(pathTestFile1);
        for (int i : instance.keySet()) {
            assertEquals(instance.get(i), ctrl.getPolygonName(i));
        }
    }

    /******************************************************************************************************************
     * ALINEA C)
     ******************************************************************************************************************/

    /**
     * Testa arvore devolvida com todos os nomes de poligonos com lados entre 1~999
     *
     * @throws IOException lanca excepcao quando ha erro IO
     */
    @Test
    public void getPolygonTree() throws IOException {
        Map<Integer, String> instance = importer.importTestFileSidesNames(pathTestFile1);
        int size = 0;
        for (Object o : ctrl.getPolygonTree().inOrder()) {
            assertTrue(instance.values().contains(((Prefix) o).getKey()));
            size++;
        }
        assertEquals(size, instance.values().size());
    }

    /******************************************************************************************************************
     * ALINEA D)
     ******************************************************************************************************************/

    /**
     * Testa lado devolvido para nome de poligono passado como argumento
     *
     * @throws IOException lanca excepcao quando ha erro IO
     */
    @Test
    public void getSidesPolygon() throws IOException {
        Map<Integer, String> instance = importer.importTestFileSidesNames(pathTestFile1);
        ;
        for (int i = 1; i < instance.size(); i++) {
            assertEquals(i, ctrl.getSidesPolygon(instance.get(i)));
        }
    }

    /******************************************************************************************************************
     * ALINEA E)
     ******************************************************************************************************************/

    /**
     * Devolve lista com nome de poligonos no intervalo
     *
     * @throws IOException lanca excepcao quando ha erro IO
     */
    @Test
    public void getNamesPolygon() throws IOException {
        Map<Integer, String> instance = importer.importTestFileSidesNames(pathTestFile1);
        int start = 1, end = 9;
        LinkedList result = (LinkedList) ctrl.getNamesPolygon(start, end);
        for (int i = end; i > start - 1; i--) {
            assertEquals(result.pop(), instance.get(i));
        }

        start = 10;
        end = 99;
        result = (LinkedList) ctrl.getNamesPolygon(start, end);
        for (int i = end; i > start - 1; i--) {
            assertEquals(result.pop(), instance.get(i));
        }

        start = 100;
        end = 999;
        result = (LinkedList) ctrl.getNamesPolygon(start, end);
        for (int i = end; i > start - 1; i--) {
            assertEquals(result.pop(), instance.get(i));
        }
    }

    /******************************************************************************************************************
     * ALINEA E)
     ******************************************************************************************************************/

    /**
     * Testa Lowest Common Ancestor
     */
    @Test
    public void findLCA() {
        assertEquals("Result should be 330",
                ctrl.findLCA("trihectatriacontahenagon", "trihectatriacontadigon").getValue(), 330);
        assertEquals("Result should be 12",
                ctrl.findLCA("hectagon", "decagon").getValue(), 12);
    }
}