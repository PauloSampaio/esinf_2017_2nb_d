package utils;

import controllers.PolygonsCtrl;
import models.Prefix;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * Classe de teste de importacao de dados
 */
public class DataImporterTest {
    private DataImporter importer;
    private PolygonsCtrl ctrl;
    private String pathUnitsFile, pathDozensFiles, pathHundredsFile;

    /**
     * Setup da classe de testes
     *
     * @throws IOException excepcao de erro IO na leitura de ficheiros
     */
    @Before
    public void setUp() throws IOException {
        importer = new DataImporter();
        ctrl = new PolygonsCtrl();
        pathUnitsFile = "src/test/files/poligonos_prefixo_unidades.txt";
        pathDozensFiles = "src/test/files/poligonos_prefixo_dezenas.txt";
        pathHundredsFile = "src/test/files/poligonos_prefixo_centenas.txt";

        importer.importPrefixes(pathUnitsFile, ctrl);
        importer.importPrefixes(pathDozensFiles, ctrl);
        importer.importPrefixes(pathHundredsFile, ctrl);
    }

    /******************************************************************************************************************
     * ALINEA A)
     ******************************************************************************************************************/

    /**
     * Testa importacao de dados para estruturas do controller
     */
    @Test
    public void importPrefixes() {
        // Teste importacao de prefixos - Unidades
        assertTrue(ctrl.contains(new Prefix(1, "hena")));
        assertTrue(ctrl.contains(new Prefix(2, "di")));
        assertTrue(ctrl.contains(new Prefix(3, "tri")));

        // Teste importacao de prefixos - Dezenas
        assertTrue(ctrl.contains(new Prefix(10, "deca")));
        assertTrue(ctrl.contains(new Prefix(11, "hendeca")));
        assertTrue(ctrl.contains(new Prefix(12, "dodeca")));

        // Teste importacao de prefixos - Centenas
        assertTrue(ctrl.contains(new Prefix(100, "hecta")));
        assertTrue(ctrl.contains(new Prefix(200, "dihecta")));
        assertTrue(ctrl.contains(new Prefix(300, "trihecta")));
    }
}